"""Top-level package for Test project for the Python Libraries' GitLab CI Template."""

__author__ = """Emmanuel Sciara"""
__email__ = "emmanuel.sciara@gmail.com"
from .__version__ import __version__  # noqa
