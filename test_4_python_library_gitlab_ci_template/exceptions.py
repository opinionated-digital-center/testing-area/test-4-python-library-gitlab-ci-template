"""All package specific exceptions."""


class Test4PythonLibraryGitlabCiTemplateError(Exception):
    """
    Base exception for errors raised by
    test_4_python_library_gitlab_ci_template.
    """
