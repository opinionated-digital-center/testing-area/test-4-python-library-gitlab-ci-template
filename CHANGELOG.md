# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

##[0.11.0](https://gitlab.com/opinionated-digital-center/testing-area/test-4-python-library-gitlab-ci-template/compare/v0.10.0...v0.11.0) (2020-06-10)


### Features

* 20-06-10_15h03-46_533280 ([97ccc9e](https://gitlab.com/opinionated-digital-center/testing-area/test-4-python-library-gitlab-ci-template/commit/97ccc9e3dd3853642b30759db8d746b774c24f97))

##[0.10.0](https://gitlab.com/opinionated-digital-center/testing-area/test-4-python-library-gitlab-ci-template/compare/v0.9.0...v0.10.0) (2020-06-10)


### Features

* 20-06-10_14h35-49_272628 ([83c2a9a](https://gitlab.com/opinionated-digital-center/testing-area/test-4-python-library-gitlab-ci-template/commit/83c2a9a9ca45b20a13a9097a86fc44141163023d))
* 20-06-10_14h45-53_250348 ([4dd27d4](https://gitlab.com/opinionated-digital-center/testing-area/test-4-python-library-gitlab-ci-template/commit/4dd27d4c14687ab8decbb71cec0f5e0b7eeaa4be))

##[0.9.0](https://gitlab.com/opinionated-digital-center/testing-area/test-4-python-library-gitlab-ci-template/compare/v0.8.0...v0.9.0) (2020-06-10)


### Features

* 20-06-10_10h44-25_062718 ([c16fec4](https://gitlab.com/opinionated-digital-center/testing-area/test-4-python-library-gitlab-ci-template/commit/c16fec4eb4f2efe29c38881c23002bcb66858ce1))

##[0.8.0](https://gitlab.com/opinionated-digital-center/testing-area/test-4-python-library-gitlab-ci-template/compare/v0.7.0...v0.8.0) (2020-06-10)


### Features

* 20-06-10_09h46-43_825405 ([b81ef52](https://gitlab.com/opinionated-digital-center/testing-area/test-4-python-library-gitlab-ci-template/commit/b81ef52c32e5edd817f999c2c9766e468bcf267b))

##[0.7.0](https://gitlab.com/opinionated-digital-center/testing-area/test-4-python-library-gitlab-ci-template/compare/v0.6.0...v0.7.0) (2020-06-10)


### Features

* 20-06-10_08h13-28_761811 ([6451072](https://gitlab.com/opinionated-digital-center/testing-area/test-4-python-library-gitlab-ci-template/commit/6451072c1bd9a75cace0706140f59533a6d83278))

##[0.6.0](https://gitlab.com/opinionated-digital-center/testing-area/test-4-python-library-gitlab-ci-template/compare/v0.5.0...v0.6.0) (2020-04-30)


### Features

* 20-04-30_19h49-13_084929 ([c5446f6](https://gitlab.com/opinionated-digital-center/testing-area/test-4-python-library-gitlab-ci-template/commit/c5446f6ece15399be0b80eeca235b771bdac346a))

##[0.5.0](https://gitlab.com/opinionated-digital-center/testing-area/test-4-python-library-gitlab-ci-template/compare/v0.4.0...v0.5.0) (2020-04-30)


### Features

* 20-04-30_19h27-00_649470 ([9d47099](https://gitlab.com/opinionated-digital-center/testing-area/test-4-python-library-gitlab-ci-template/commit/9d47099a8a57f71833d18332a403e09ca9977d9e))

##[0.4.0](https://gitlab.com/opinionated-digital-center/testing-area/test-4-python-library-gitlab-ci-template/compare/v0.3.0...v0.4.0) (2020-04-29)


### Features

* 20-04-29_02h05-42_341676 ([f365e4a](https://gitlab.com/opinionated-digital-center/testing-area/test-4-python-library-gitlab-ci-template/commit/f365e4a06e875d66d4902b828269e103d3c51fff))

##[0.3.0](https://gitlab.com/opinionated-digital-center/testing-area/test-4-python-library-gitlab-ci-template/compare/v0.2.0...v0.3.0) (2020-04-29)


### Features

* 20-04-29_01h16-38_401197 ([220cfad](https://gitlab.com/opinionated-digital-center/testing-area/test-4-python-library-gitlab-ci-template/commit/220cfad7fcab2a3778e29e3eda2602bd75528160))

##[0.2.0](https://gitlab.com/opinionated-digital-center/testing-area/test-4-python-library-gitlab-ci-template/compare/v0.1.0...v0.2.0) (2020-04-29)


### Features

* 20-04-29_01h00-49_151587 ([c9ff858](https://gitlab.com/opinionated-digital-center/testing-area/test-4-python-library-gitlab-ci-template/commit/c9ff858a8c5da71efecc870cab94e6d639cec08c))

##[0.1.0](https://gitlab.com/opinionated-digital-center/testing-area/test-4-python-library-gitlab-ci-template/compare/v0.0.0...v0.1.0) (2020-04-29)


### Features

* 20-04-29_02h50-16_846167 ([bc16672](https://gitlab.com/opinionated-digital-center/testing-area/test-4-python-library-gitlab-ci-template/commit/bc166726694ea384781296aeaa4a306c31609c08))
