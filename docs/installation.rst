.. highlight:: shell

============
Installation
============


Stable release
--------------

To install Test project for the Python Libraries' GitLab CI Template, run this command in your terminal:

.. code-block:: console

    $ pip install test-project

This is the preferred method to install Test project for the Python Libraries' GitLab CI Template, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for Test project for the Python Libraries' GitLab CI Template can be downloaded
from the `GitLab repo`_.

Clone the public repository:

.. code-block:: console

    $ git clone https://gitlab.com/opinionated-digital-center/testing-area/test-4-python-library-gitlab-ci-template.git

Then install it with:

.. code-block:: console

    $ cd test-project
    $ POETRY_VIRTUALENVS_CREATE=FALSE poetry install


.. _GitLab repo: https://gitlab.com/opinionated-digital-center/testing-area/test-4-python-library-gitlab-ci-template
